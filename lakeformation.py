def lf_grant_permissions(logger, lf_client, principal: str, database_name: str, table_name: str = None,
                         permissions: list = ['ALL'],
                         grantable_permissions: list = ['ALL']):
    try:
        table_spec = {
            'DatabaseName': database_name
        }
        if table_name is None or table_name == "*":
            table_spec['TableWildcard'] = {}
        else:
            table_spec['Name'] = table_name

        args = {
            "Principal": {
                'DataLakePrincipalIdentifier': principal
            },
            "Resource": {
                'Table': table_spec
            },
            "Permissions": permissions
        }

        if grantable_permissions is not None:
            args["PermissionsWithGrantOption"] = grantable_permissions

        logger.debug(args)

        return lf_client.grant_permissions(**args)
    except lf_client.exceptions.from_code('AlreadyExistsException') as aee:
        return None
    except lf_client.exceptions.InvalidInputException as iie:
        if "Permissions modification is invalid" in str(iie):
            # this is an error thrown when you try to create the same permissions that already exist :(
            return None
        elif "Please revoke permission(s) for IAM_ALLOWED_PRINCIPALS on the table" in str(iie):
            # this occurs because we are granting any IAM principal to describe the table, which means that the previous creation of the grant is already in place. ignore
            return None
        else:
            raise iie